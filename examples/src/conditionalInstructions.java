import java.util.Scanner;

public class conditionalInstructions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        System.out.println("How old are you?");
//        int age = scanner.nextInt();
//
//        if (age >= 18)  {
//            System.out.println("You are of legal age");
//        }   else {
//            System.out.println("You are not of age");
//        }
        System.out.println("Jakie jest światło? (zielone, żółte, czerwone)");
        String light = scanner.nextLine();

        if (light.equals("zielone"))  {
            System.out.println("Światło było koloru zielonego");
        } else if (light.equals("żółte")) {
            System.out.println("światło było koloru żółtego");
        }   else {
            System.out.println("Światło było koloru czerwonego!");
        }
    }
}
