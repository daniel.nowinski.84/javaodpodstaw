package exercises.src;

import java.util.Scanner;

public class ex3_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the value in points: ");
        int score = scanner.nextInt();

        if (score > 100 || score < 0)   {
            System.out.println("Wrong value!");
            System.exit(0);
        }
        if (score >= 90) {
            System.out.println("Your rating is 5");
        } else if (score >= 80) {
            System.out.println("Your rating is 4");
        } else if (score >= 70) {
            System.out.println("Your rating is 3");
        } else if (score >= 60) {
            System.out.println("Your rating is 2");
        } else if (score < 60) {
            System.out.println("Your rating is 1!");
        }

    }
}
